//
//  ViewController.m
//  dropdown1
//
//  Created by CLI112 on 10/6/15.
//  Copyright (c) 2015 CLI112. All rights reserved.
//

#import "ViewController.h"

@interface ViewController (){
    NSArray *celldata;
 
}
@property (strong, nonatomic) IBOutlet UIButton *add;
@property (strong, nonatomic) IBOutlet UITableView *tableview;


@end

@implementation ViewController




- (void)viewDidLoad {
    [super viewDidLoad];
    celldata=@[@"param",@"hundal",@"Mani",@"Mohali",@"gagan",@"peend"];
    [_tableview reloadData];
    _tableview.hidden=YES;
    
    
    // Do any additional setup after loading the view, typically from a nib.
}

-(IBAction)add:(id)sender
{
    _tableview.hidden=YES;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"sell"];
    cell.textLabel.text=celldata[indexPath.row];
    return cell;
    
                           
}

-(NSInteger)numberofsectioninTableView:(UITableView*)tableview;

{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return celldata.count;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    NSString *str=celldata[indexPath.row];
    [_add setTitle:str  forState:UIControlStateNormal];
    _tableview.hidden=YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
